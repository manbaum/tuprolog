%=======================================================================
\chapter{\tuprolog{} Basics}
\label{ch:engine}
%=======================================================================
%
% General stuff about 2P engine features
%
%

\noindent This chapter provides a brief introduction to the basic elements and structure of the \tuprolog{} engine, covering syntax, programming support, and built-in predicates directly provided by the engine.

%---------------------------------------------------------------------
\section{Structure of a \tuprolog{} Engine}
%---------------------------------------------------------------------

\noindent A \tuprolog{} engine has a layered structure, where provided and recognised predicates are organised into three different categories:
%
\begin{description}
\item[built-in predicates] |
Predicates embedded in any \tuprolog{} engine are called built-in predicates.
%
Whatever modification is made to the engine either before or during execution time, it does not affect the number and properties of the built-in predicates.
%
\item[library predicates] |
Predicates loaded in a \tuprolog{} engine by means of a \tuprolog{} library are called library predicates.
%
Since libraries can be loaded and unloaded in \tuprolog{} engines freely at the system start-up, or dynamically at execution time, the set of the library predicates of a \tuprolog{} engine is not fixed, and can change from engine to engine, and in the same engine at different times.
%
\tuprolog{} libraries can be built by mixing Java and Prolog code. Prolog
library predicates can be overridden by Prolog theory predicates. Both Java and
Prolog library predicates cannot be individually retracted: if you want to
remove a single library predicate from the engine, you need to unload the whole
library containing that predicate.
\item[theory predicates] |
Predicates loaded in a \tuprolog{} engine by means of a \tuprolog{} theory are called theory predicates.
%
Since theories can be loaded and unloaded in \tuprolog{} engines freely at the system start-up, or dynamically at execution time, the set of the theory predicates of a \tuprolog{} engine is not fixed, and can change from engine to engine, and in the same engine at different times. 
%
\tuprolog{} theories are simple collections of Prolog clauses.
\end{description}
%
Even though they may seem similar, library and theory predicates are handled differently in a \tuprolog{} engine.

First of all, they are conceptually different. In fact, while theory predicates should be used to axiomatically represent domain knowledge at the time the proof is performed, library predicates should more or less be used to represent what is required (procedural knowledge, utility predicates) in order to actually and effectively perform a (number of) proof(s) in the domain of interest: therefore, library predicates represent more ``stable'' knowledge, which is encapsulated once and for all (at least approximately) within a library container.

Since library and theory predicates are also structurally different, they are handled differently by the engine, and represented differently in the run-time: correspondingly, they have different level of observation when monitoring or debugging a working \tuprolog{} engine.
%
As a consequence, developer tools provided by \tuprolog{} IDE typically show in a separate way the theory axioms or predicates and the loaded libraries or predicates.
%
In addition, the debugging phase typically neglects library predicates (which, as mentioned above, are also conceived as more stable and well-tested), while the effect of the theory predicates is dutifully put in evidence during controlled execution.

%---------------------------------------------------------------------
\section{Prolog syntax}
%---------------------------------------------------------------------

\noindent The term syntax supported by \tuprolog{} engine is basically ISO compliant,\footnote{Currently ISO exceptions, ISO I/O predicates and some ISO directives are not supported.}
and accounts for several elements:
%
\begin{description}
\item[Comments and Whitespaces] -- Whitespaces consist of blanks (including tabs and formfeeds), end-of-line marks, and comments. A whitespace can be put before and after any term, operator, bracket, or argument separator, as long as it does not break up an atom or number or separate a functor from the opening parenthesis that introduces its argument lists.
%
For instance, atom \bt{p(a,b,c)} can be written as \bt{p(\mbox{~a~},\mbox{~b~},\mbox{~c~})}, but not as \bt{\mbox{p~}(a,b,c)}).
%
Two types of comments are supported: one type begins with \bt{/*} and ends with \bt{*/}, the other begins with \bt{\%} and ends at the end of the line.
%
Nested comments are not allowed.
%
\item[Variables] |
A variable name begins with a capital
letter or the underscore mark (\bt{\_}), and consists of letters,
digits, and/or underscores.
%
A single underscore mark denotes an anonymous variable.
%
\item[Atoms] |
There are four types of atoms:
\emph{(i)} a series of letters, digit, and/or underscores, beginning with a lower-case letter; \emph{(ii)} a series of one or more characters from the set \{\texttt{\#}, \texttt{\$}, \texttt{\&}, \texttt{*}, \texttt{+}, \texttt{-}, \texttt{.}, \texttt{/}, \texttt{:}, \texttt{<}, \texttt{=}, \texttt{>}, \texttt{?}, \texttt{@}, \texttt{\textasciicircum}, \texttt{\~}\}, provided it does not begin with \texttt{/*};
\emph{(iii)} The special atoms \texttt{[]} and \texttt{\{\}};
\emph{(iv)} a single-quoted string.
%
\item[Numbers] |
Integers and float are supported.
%
The formats supported for integer numbers are decimal, binary (with \verb|0b|
prefix), octal (with \verb|0o| prefix), and hexadecimal (with \verb|0x|
prefix). The character code format for integer numbers (prefixed by \verb|0'|) is supported only for alphanumeric characters, the white space, and characters in the set \{\texttt{\#}, \texttt{\$}, \texttt{\&}, \texttt{*}, \texttt{+}, \texttt{-}, \texttt{.}, \texttt{/}, \texttt{:}, \texttt{<}, \texttt{=}, \texttt{>}, \texttt{?}, \texttt{@}, \texttt{\textasciicircum}, \texttt{\~}\}.
%
The range of integers is -2147483648 to 2147483647; the range of floats is
-2E+63 to 2E+63-1.
%
Floating point numbers can be expressed also in the exponential format (e.g. \bt{-3.03E-05}, \bt{0.303E+13}).
%
A minus can be written before any number to make it negative (e.g. \bt{-3.03}).
%
Notice that the minus is the sign-part of the number itself; hence \bt{-3.4} is a number, not an expression (by contrast, \bt{- 3.4} is an expression).
%
\item[Strings] |
A series of ASCII characters, embedded in quotes \verb|'| or \verb|"|.
%
Within single quotes, a single quote is written double (e.g, \verb|'don''t forget'|).
%
A backslash at the very end of the line denotes continuation to the next line, so that: \\
\verb|'this is \ |\\
\verb|a single line'|\\
is equivalent to \verb|'this is a single line'| (the line break is ignored).
%
Within a string, the backslash can be used to denote special characters, such as \verb|\n| for a newline,
\verb|\r| for a return without newline,
\verb|\t| for a tab character,
\verb|\\| for a backslash,
\verb|\'| for a single quote,
\verb|\"| for a double quote.
%
\item[Compounds] |
The ordinary way to write a compound is to write the functor (as an atom), an opening parenthesis, without spaces between them, and then a series of terms separated by commas, and a closing parenthesis: \bt{f(a,b,c)}.
%
This notation can be used also for functors that are normally written as operators, e.g. \bt{2+2} = \verb|'+'(2,2)|.
%
Lists are defined as rightward-nested structures using the dot operator \verb|'.'|; so, for example: \\
\bt{[a] =} \verb|'.'(a,[])|\\
\bt{[a,b] =} \verb|'.'(a,'.'(b,[]))|\\
\bt{[a,b|c] =} \verb|'.'(a,'.'(b,c))|\\
%
There can be only one \bt{|} in a list, and no commas after it.
%
Also curly brackets are supported: any term enclosed with \bt{$\{$} and \bt{$\}$} is treated as the argument of the special functor \verb|'{}'|:  \verb|{hotel}| = \verb|'{}'(hotel)|, \bt{$\{$1,2,3$\}$} = \verb|'{}'(1,2,3)|.
%
Curly brackets can be used in the Definite Clause Grammars theory.

\item[Operators] |
Operators are characterised by a name, a specifier, and a priority.
%
An operator name is an atom, which is not univocal: the same atom can be an operator in more than one class, as in the case of the infix and prefix minus signs.
%
An operator  specifier is a string like \texttt{xfy}, which gives both its class (infix, postfix and prefix) and its associativity: \texttt{xfy} specifies that the grouping on the right should be formed first, \texttt{yfx} on the left, \texttt{xfx} no priority.
%
An operator priority is a non-negative integer ranging from 0 (max priority) and 1200 (min priority).

Operators can be defined by means of either the \bt{op/3} predicate or directive.
%
No predefined operators are directly given by the raw \tuprolog{} engine, whereas a number of them is provided through libraries.
%
\item[Commas] |
The comma has three functions: it separates arguments of functors, it separates elements of lists, and it is an infix operator of priority 1000.
%
Thus \bt{(a,b)} (without a functor in front) is a compound, equivalent to \verb|','(a,b)|.
%
\item[Parenthesis] -- Parenthesis are allowed around any term.
%
The effect of parenthesis is to override any grouping that may
otherwise be imposed by operator priorieties.
%
Operators enclosed in parenthesis do not function as operators;
thus \bt{2(+)3} is a syntax error.
\end{description}

%---------------------------------------------------------------------
\section{Configuration of a \tuprolog{} Engine}
%---------------------------------------------------------------------
\noindent Prolog developers have four different means to configure a \tuprolog{} engine in order to fit their application needs.
%
In fact, a \tuprolog{} can be suitably configured by means of:

\begin{description}
%
\item[Theories] |
A \tuprolog{} theory is represented by a text, consisting of a sequence of clauses and/or directives.
%
Clauses and directives are terminated by a dot, and are separated by a whitespace character.
%
Theories can be loaded or unloaded by means of suitable library predicates, which are described in Chapter \ref{ch:standard-libraries}.
%
\item[Directives] |
A directive can be given by means of the \bt{:-/1} predicate, which is natively supported by the engine, and can be used to configure and use a \tuprolog{} engine (\bt{set\_prolog\_flag/1}, \bt{load\_library/1}, \bt{consult/1}, \bt{solve/1}), format and syntax of read-terms\footnote{As specified by the ISO standard, a read-term is a Prolog term followed by an end token, composed by an optional layout text sequence and a dot.} (\bt{op/3}).
%
Directives are described in detail in the following sections.
%
\item[Flags] |
A \tuprolog{} engine allows the dynamic definition of flags (or properties) describing some aspects of libraries and their predicates and evaluable functors.
%
A flag is identified by a name (an alphanumeric atom), a list of possible values, a default value, and a boolean value specifying if the flag value can be modified.
%
Dynamically, a flag value can be changed (if modifiable) with a new value included in the list of possible values.
%
\item[Libraries] |
A \tuprolog{} engine can be dynamically extended by loading or unloading libraries.
%
Each library can provide a specific set of predicates, functors, and a related theory, which also allows new flags and operators to be defined.
%
Libraries can be either pre-defined (see Chapter \ref{ch:standard-libraries}) or user-defined (see Chapter \ref{ch:howto-develop-libraries}).
%
A library can be loaded by means of the predicate \texttt{load\_library} (Prolog side), or by means of the method \texttt{loadLibrary} of the \tuprolog{} engine (Java side).
\end{description}
%
Currently \tuprolog{} does not support exception management: actually, an exception causes the predicate/functor in which it occurred to fail and be false.
%

%---------------------------------------------------------------------
\section{Built-in predicates}
%---------------------------------------------------------------------

\noindent This section contains a comprehensive list of the built-in predicates provided by the \tuprolog{} engine, that is, those predicates defined directly in its core.

Following an established convention in built-in argument template description, which takes root into an imperative interpretation, the symbol \bt{+} in front of an argument means an \emph{input argument}, \bt{-} means \emph{output argument}, \bt{?} means \emph{input/output} argument, \bt{@} means \emph{input argument} that must be bound.

%---------------------------------------------------------------------
\subsection{Control management}
%---------------------------------------------------------------------

\begin{itemize}
%
\item \bti{true/0}\\
\noindent\bt{true} is true.
%
\item \bti{fail/0}\\
\noindent\bt{fail} is false.
%
\item \verb|','/2|\\
\noindent\verb|','(First,Second)| is true if and only if both \bt{First}
and \bt{Second} are true.
%
\item \bti{!/0}\\
\noindent\bt{!} is true. All choice points between the cut and the
parent goal are removed. The effect is a commitment to use both the
current clause and the substitutions found at the point of the
cut.
%
\item \verb|'$call'/1|\\
\noindent\verb|'$call'(Goal)| is true if and only if \bt{Goal}
represents a goal which is true. It is not opaque to cut.\\
\template{'\$call'(+callable\_term)}
%
\item \bti{halt/0}\\
\noindent\bt{halt} terminates a Prolog demonstration, exiting the
Prolog processor and returning to the system that invoked the
processor.
%
\item \bti{halt/1}\\
\noindent\bt{halt(X)} terminates a Prolog demonstration, exiting
the Prolog processor and returning to the systems that invoked the
processor passing the value of \bt{X} as a message.\\
\template{halt(+int)}
%
\end{itemize}

%---------------------------------------------------------------------
\subsection{Term Unification and Management}
%---------------------------------------------------------------------

\begin{itemize}
%
\item \bti{is/2}\\
\noindent\bt{is(X, Y)} is true iff \bt{X} is unifiable with the
value of the expression \bt{Y}.\\
\noindent\template{is(?term, @evaluable)}
%
\item \verb|'='/2|\\
\noindent\verb|'='(X, Y)| is true iff \bt{X} and \bt{Y} are
unifiable.\\
\noindent\template{'='(?term, ?term)}
%
\item \verb|'\='/2|\\
\noindent\verb|'\='(X, Y)| is true iff \bt{X} and \bt{Y} are
not unifiable. \\
\noindent\template{'$\setminus$='(?term, ?term)}
%
%
\item \verb|'$tolist'/2|\\
\noindent\verb|'$tolist'(Compound, List)| is true if \bt{Compound} is
a compound term, and in this case \bt{List} is list representation
of the compound, with the name as first element and all the
arguments as other elements.\\
\noindent\template{'\$tolist'(@struct, -list)}
%
 \item \verb|'$fromlist'/2|\\
 \noindent\verb|'$fromlist'(Compound, List)| is true if \bt{Compound}
 unifies with the list representation of \bt{List}.\\
\noindent\template{'\$fromlist'(-struct, @list)}
%
 \item \bti{copy\_term/2}\\
 \noindent\bt{copy\_term(Term1, Term2)} is true iff \bt{Term2}
 unifies with the a renamed copy of \bt{Term1}.\\
\noindent\template{copy\_term(?term, ?term)}
%
 \item \verb|'$append'/2|\\
 \noindent\verb|'$append'(Element, List)| is true if \bt{List} is a
 list, with the side effect that the \bt{Element} is appended to
 the list.\\
\noindent\template{'\$append'(+term, @list)}
%
\end{itemize}

%---------------------------------------------------------------------
\subsection{Knowledge-base management}
%---------------------------------------------------------------------

\begin{itemize}
%
 \item \verb|'$find'/2|\\
 \noindent\verb|'$find'(Clause, ClauseList)| is true if \bt{ClauseList}
 is a list, and \bt{Clause} is a clause, with the side effect that
 all the clauses of the database matching \bt{Clause} are
 appended to the list.\\
\noindent\template{'\$find'(@clause, @list)}
%
\item \bti{abolish/1}\\
\noindent\bt{abolish(PI)} completely wipes out the dynamic
predicate matching the predicate indicator \texttt{PI}.\\
\noindent\template{\bt{abolish(@term)}}
%
\item \bti{asserta/1}\\
\noindent\bt{asserta(Clause)} is true, with the side effect that
the clause \bt{Clause} is added to the beginning of database.\\
\noindent\template{asserta(@clause)}
%
\item \bti{assertz/1}\\
\noindent\bt{assertz(Clause)} is true, with the side effect that
the clause \bt{Clause} is added to the end of the database.\\
\noindent\template{assertz(@clause)}
%
\item \verb|'$retract'/1|\\
\noindent\verb|'$retract'(Clause)| is true if the database contains
at least one clause unifying with \bt{Clause}. As a side effect, the
clause is removed from the database. It is not re-executable.\\
\noindent\template{'\$retract'(@clause)}
%
\end{itemize}

%---------------------------------------------------------------------
\subsection{Operators and Flags Management}
%---------------------------------------------------------------------

\begin{itemize}
%
\item \bti{op/3}\\
\noindent\bt{op(Priority, Specifier, Operator)} is true. It always succeeds,
modifying the operator table as a side effect. If \bt{Priority} is 0, then
\bt{Operator} is removed from the operator table; else, \bt{Operator} is
added to the operator table, with priority (lower binds tighter) \bt{Priority}
and associativity determined by \bt{Specifier}. If an operator with the same
\bt{Operator} symbol and the same \bt{Specifier} already exists in the operator
table, the predicate modifies its priority according to the specified \bt{Priority}
argument.\\
\noindent\template{op(+integer, +specifier, @atom\_or\_atom\_list)}
%
%  \item \bti{flag/2}\\
%  \noindent\bt{flag(FlagName, NewValue)} is true if \bt{FlagName} is
%  the name of a modifiable flag currently defined in the engine
%  and \bt{NewValue} is a valid value for the flag. As a side effect,
%  \bt{NewValue} becomes the new value of the flag \bt{FlagName}.\\
%  \noindent\template{flag(@string, @term)}
%
 \item \bti{flag\_list/1}\\
 \noindent\bt{flag\_list(FlagList)} is true and \bt{FlagList} is
 the list of the flags currently defined in the engine.\\
 \noindent\template{flag\_list(-list)}
%
\item \bti{set\_prolog\_flag/2}\\
\noindent\bt{set\_prolog\_flag(Flag, Value)} is true, and as a side
effect associates \bt{Value} with the flag \bt{Flag}, where
\bt{Value} is a value that is within the implementation defined
range of values for \bt{Flag}.\\
\noindent\template{set\_prolog\_flag(+flag, @nonvar)}
%
\item \bti{get\_prolog\_flag/2}\\
\noindent\bt{get\_prolog\_flag(Flag, Value)} is true iff \bt{Flag}
is a flag supported by the engine and \bt{Value} is the value
currently associated with it. Note that \bt{get\_prolog\_flag/2} is
not re-executable.\\
\noindent\template{get\_prolog\_flag(+flag, ?term)}
%
\end{itemize}

%---------------------------------------------------------------------
\subsection{Libraries Management}
%---------------------------------------------------------------------

\begin{itemize}
%
 \item \bti{load\_library/1}\\
 \noindent\bt{load\_library(LibraryName)} is true if
 \bt{LibraryName} is the name of a \tuprolog{} library available
 for loading. As side effect, the specified library is loaded by
 the engine. Actually \bt{LibraryName} is the full name of
 the Java class providing the library.\\
 \noindent\template{load\_library(@string)}
%
 \item \bti{unload\_library/1}\\
 \noindent\bt{unload\_library(LibraryName)} is true if
 \bt{LibraryName} is the name of a library currently loaded in the
 engine. As side effect, the library is unloaded from the engine. Actually \bt{LibraryName} is the full name of
 the Java class providing the library.\\
 \noindent\template{unload\_library(@string)}
%
\end{itemize}

%---------------------------------------------------------------------
\subsection{Directives}
%---------------------------------------------------------------------

Directives are used in Prolog text only as queries to be immediately executed when loading it. When a corresponding predicate with the same procedure name as a directive exists, they perform the same actions. Their arguments will satisfy the same constraints as those required for an errorless execution of the corresponding predicate, otherwise their behaviour is undefined.

In \tuprolog{}, directives are not composable: each query must contain one and only one directive. When you need to use multiple directives, you must employ multiple queries as well.

\begin{itemize}
 %
 \item \bti{:- op/3}\\
 \noindent\bt{op(Priority, Specifier, Operator)} adds \bt{Operator}
 to the operator table, with priority (lower binds tighter)
 \bt{Priority} and associativity determined by \bt{Specifier}.\\
 \noindent\template{op(+integer, +specifier, @atom\_or\_atom\_list)}
 %
 \item \bti{:- flag/4}\\
 \noindent\bt{flag(FlagName, ValidValuesList, DefaultValue, IsModifiable)}
 adds to the engine a new flag, identified by the \bt{FlagName}
 name, which can assume only the values listed in
 \bt{ValidValuesList} with \bt{DefaultValue} as default value, and
 that can be modified if \bt{IsModifiable} is true.\\
 \noindent\template{flag(@string, @list, @term, @{true, false})}
 %
 \item \bti{:- initialization/1}\\
 \noindent\bt{initialization(Goal)} sets the starting goal to be executed just
 after the theory has been consulted.\\
 \noindent\template{initialization(@goal)}
 %
 \item \bti{:- solve/1}\\
 \noindent Synonym for \bt{initialization/1}. \emph{Deprecated.}\\
 \noindent\template{solve(@goal)}
 %
 \item \bti{:- load\_library/1}\\
 \noindent\bt{load\_library(LibraryName)} is a valid directive if true if
 \bt{LibraryName} is the name of a \tuprolog{} library available
 for loading. This directive loads the specified library in the engine.
 Actually \bt{LibraryName} is the full name of the Java class providing the library.\\
 \noindent\template{load\_library(@string)}
 %
 \item \bti{:- include/1}\\
 \noindent\bt{include(Filename)} immediately loads the theory
 contained in the file specified by \bt{Filename}.\\
 \noindent\template{include(@string)}
 %
 \item \bti{:- consult/1}\\
 \noindent Synonym for \bt{include/1}. \emph{Deprecated.}\\
 \noindent\template{consult(@string)}
 %
\end{itemize}
%
